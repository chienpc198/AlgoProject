import "reflect-metadata"
import { DataSource } from "typeorm"
import { TaiKhoan } from "./entity/TaiKhoan"

export const AppDataSource = new DataSource({
    type: "postgres",
    host: "localhost",
    port: 8060,
    username: "postgres",
    password: "190800",
    database: "myProject",
    synchronize: true,
    logging: false,
    entities: [TaiKhoan],
    migrations: [],
    subscribers: [],
})
