import { 
  Entity, 
  PrimaryGeneratedColumn, 
  Column,
  OneToMany
} from "typeorm"
import { BaiViet } from './BaiViet'

@Entity()
export class Nhom {

    @PrimaryGeneratedColumn()
    sMaNhom: string

    @Column()
    sTenNhom: string

    @OneToMany(() => BaiViet, (baiViet) => baiViet.nhom)
    baiViet: BaiViet[]

}
