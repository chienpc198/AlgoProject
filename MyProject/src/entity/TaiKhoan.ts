import { type } from 'os'
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToMany
} from "typeorm"
import { BinhLuan } from './BinhLuan'

@Entity()
export class TaiKhoan {

    @PrimaryGeneratedColumn()
    iMaTaiKhoan: number

    @Column()
    sTenTaiKhoan: string

    @Column()
    sMatKhau: string

    @Column()
    sEmail: string

    @Column()
    iQuyen: number

    @OneToMany(() => BinhLuan, (binhLuan) => binhLuan.taiKhoan)
    binhLuans: BinhLuan[]

}
