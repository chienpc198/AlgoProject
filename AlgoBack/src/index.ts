import { AppDataSource } from "./data-source"
import { TaiKhoan } from "./entity/TaiKhoan"
const express = require('express')


AppDataSource.initialize().then(async () => {

    const users = await AppDataSource.manager.find(TaiKhoan)
    console.log("Loaded users: ", users)

    console.log('Connected to database');
    

}).catch(error => console.log(error))

const app = express()

app.listen(8061, () => {
    console.log('Listening on port 8061');
    
})