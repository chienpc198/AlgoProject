import { 
  Entity, 
  PrimaryGeneratedColumn, 
  Column,
  ManyToOne
} from "typeorm"
import { TaiKhoan } from './TaiKhoan'

@Entity()
export class BinhLuan {

    @PrimaryGeneratedColumn()
    sMaBinhLuan: string

    @Column()
    sNoiDungBinhLuan: string

    @ManyToOne(() => TaiKhoan, (taiKhoan) => taiKhoan.binhLuans)
    taiKhoan: TaiKhoan

}
