import { 
  Entity, 
  PrimaryGeneratedColumn, 
  Column,
  ManyToOne
} from "typeorm"

@Entity()
export class Code {

    @PrimaryGeneratedColumn()
    sMaBaiViet: string

    @Column()
    sTenBaiViet: string

    @Column()
    sText: string

    @Column()
    sCode: string

    @ManyToOne(() => Nhom, (nhom) => nhom.baiViet)
    nhom: Nhom

}
