import { 
  Entity, 
  PrimaryGeneratedColumn, 
  Column,
  ManyToOne
} from "typeorm"
import { Nhom } from './Nhom'

@Entity()
export class BaiViet {

    @PrimaryGeneratedColumn()
    sMaBaiViet: string

    @Column()
    sTenBaiViet: string

    @Column()
    sText: string

    @Column()
    sCode: string

    @ManyToOne(() => Nhom, (nhom) => nhom.baiViet)
    nhom: Nhom

}
